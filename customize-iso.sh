#!/bin/bash
set -o errexit -o nounset -o pipefail -o xtrace

# Downloads the latest release of the Fedora CoreOS live ISO image and customizes it with an ignition configuration.
#
# Usage:
# ./customize-iso.sh </dev/USB_DISK_TO_OVERWRITE> [<BUTANE_CONFIG.bu> <NETWORK_KEYFILE.nmconnection>]
#
# Example:
# ./customize-iso.sh /dev/sdb bluebottle-bootstrap.bu bluebottle-network.ini
#
# WARNING: Ensure that the device you specify is the correct USB disk you want to completely overwrite!

USB_DISK_TO_OVERWRITE=$1
BUTANE_CONFIG="${2:-bluebottle-bootstrap.bu}"
NETWORK_KEYFILE="${3:-bluebottle.nmconnection}"

TEMP_DIR=`mktemp --directory`
CUSTOMIZED_IGN="$TEMP_DIR"/customized.ign
CUSTOMIZED_ISO="$TEMP_DIR"/customized.iso

# Convert the butane configuration that loads the real configuration into ignition format.
butane \
  --pretty \
  --strict \
  --output="$CUSTOMIZED_IGN" \
  "$BUTANE_CONFIG"

# Validate the ignition configuration.
ignition-validate "$CUSTOMIZED_IGN"

# Download the latest, live ISO.
# TODO: Go back to `--stream=stable when podman 4.1 gets there.
coreos-installer download \
  --format=iso \
  --stream=testing\
  --platform=metal \
  --architecture=x86_64 \
  --directory="$TEMP_DIR"

# Customize the ISO with the ignition configuration.
coreos-installer iso customize \
  --live-karg-append="rd.neednet=1" \
  --network-keyfile="$NETWORK_KEYFILE" \
  --live-ignition "$CUSTOMIZED_IGN" \
  --output "$CUSTOMIZED_ISO" \
  "$TEMP_DIR"/*.iso

# Output the location of the iso so the caller can dd it to a boot disk.
echo "About to overwrite ${USB_DISK_TO_OVERWRITE} with ${CUSTOMIZED_ISO}!"
sudo dd if="$CUSTOMIZED_ISO" of="$USB_DISK_TO_OVERWRITE" bs=1024k status=progress

rm -fr "$TEMP_DIR"
