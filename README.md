# Blue Bottle

Blue Bottle (`bluebottle`) is the name of an experiment in provisioning
infrastructure at my home lab. It's a machine that boots from a read-only ISO
on a USB disk. The image is Fedora CoreOS customized with an Ignition
configuration generated from the `bluebottle-bootstrap.bu` Butane configuration.
It would be tedious to reflash the disk everytime that I want to make a change
to the configuration, so that configuration is the minimum required to download
and apply the actual configuration generated from `bluebottle.bu` by gitlab.com
CI/CD builds and published to
https://jdmarble.gitlab.io/bluebottle/bluebottle.ign.
Since I want this device to host DHCP and tFTP servers for other hosts to boot
from, I need it to be on a seperate network from my other home DHCP server.
To configure it with a static IP address, I could put something in
`bluebottle.bu`, but that configuration is only available after the network is
already configured. Instead, the ISO is also customized with a Network Manager
"keyfile" that ensures the system boots with a properly configured, static IP
address.

## Troubleshooting

### Booting

To debug problems around dracut, interrupt the GRUB menu with `e` and use
[the instructions in the Fedora documentation](https://docs.fedoraproject.org/en-US/quick-docs/debug-dracut-problems/#identifying-your-problem-area).

### Force Filesystem Wipe

If you are provisioning a machine that has existing paritions on `/dev/sda`,
you may get the following ignition error on boot:

```
filesystem at "/dev/disks/by-partlabel/var" is not of the correct type, label, or UUID ... and a filesystem wipe was not requested
```

If you are sure that you want to overwrite the data on the disk, then you can
from the dracut emergency shell with something like this:

```sh
$ dd if=/dev/zero of=/dev/sda bs=4096 count=1024
```

### After Boot

You can [look at ignition logs](https://coreos.github.io/ignition/getting-started/#troubleshooting) if you can access the system after boot.

```sh
journalctl --identifier=ignition --all
```
